/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

import java.sql.Date;

/**
 *
 * @author Caelan
 */
public class MaintenanceReport {
    public String id;
    public String address;
    public String unit;
    public String name;
    public String type;
    public Date date;
    public String duration;
    public double cost;
    
 

    public MaintenanceReport(String id,String address,String unit,String name,String type,Date date,String duration,double cost) {
    this.id = id;
    this.unit = unit;
    this.address=address;
    this.name=name;
    this.type=type;
    this.date=date;
    this.duration=duration;
    this.cost=cost;
    }

    public MaintenanceReport() {
    }

    public String getId(){
        return this.id;
    }
    
    public String getAddress(){
        return this.address;
    }
    
    public String getUnit(){
        return this.unit;
    }
    
    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }
    public Date getDate() {
        return this.date;
    }
    public String getDuration() {
        return this.duration;
    }
    public double getCost() {
        return this.cost;
    }
  
    public void setId(String id) {
        this.id = id;
    }
    
    public void setAddress(String address){
        this.address = address;
    }
    
    public void setUnit(String unit){
        this.unit=unit;
    }
    
    public void setName(String name) {
        this.name = name;
    }

  
     public void setType(String type) {
        this.type=type;
    }
    public void setDate(Date date) {
        this.date=date;
    }
    public void setTime(String duration) {
        this.duration=duration;
    }
    public void setCost(double cost) {
        this.cost=cost;
    }
}
