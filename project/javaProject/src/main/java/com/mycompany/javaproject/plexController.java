/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

/**
 *
 * @author Caelan
 */



import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class plexController {

    
    @FXML
    private TableView<PlexReport> plexReport;
        
    @FXML
    private TableColumn<PlexReport, String> addressCol;

    @FXML
    private TableColumn<PlexReport, String> unitCol;

    @FXML
    private TableColumn<PlexReport, String> typeCol;

    @FXML
    private TableColumn<PlexReport, String> nameCol;

    @FXML
    private TableColumn<PlexReport, String> priceCol;

    @FXML
    private TableColumn<PlexReport, String> mortstartCol;
    
    @FXML
    private TableColumn<PlexReport, String> mortendCol;

    @FXML
    private TableColumn<PlexReport, String> institutionCol;

    @FXML
    private TextField address;

    @FXML
    private TextField unit;
    


    @FXML
    public TextField price;

    @FXML
    public DatePicker start;

    @FXML
    public DatePicker end;

    @FXML
    public ChoiceBox institution;
    
    @FXML
    public Label warning;

    @FXML
    public ChoiceBox type;
    
    ObservableList<String> typeList = FXCollections.observableArrayList("Duplex","Triplex","Quadraplex","Multiplex");


    @FXML
    private void switchToContractor() throws IOException {
        App.setRoot("contractorView");
    }

    @FXML
    private void switchToFinancialInstitutions() throws IOException {
        App.setRoot("financialinstitution");
    }

    @FXML
    private void switchToMaintenance() throws IOException {
        App.setRoot("maintenanceView");
    }

    @FXML
    void switchToProperties() throws IOException {
        App.setRoot("propertyView");
    }

    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("tenantView");
    }

    public void initialize(){

        type.setItems(typeList);
    }
    
        public void submitPlex(ActionEvent actionEvent)throws SQLException{
     insertRecord();
    }
    
    private void insertRecord() throws SQLException{
        
        
        if(address.getText().isEmpty() && unit.getText().isEmpty() && type.getValue() == null  && price.getText().isEmpty() && start.getValue() == null && end.getValue() == null && institution.getValue() == null)
        {
            warning.setText("one or more field is blank");
        }
        else
        {
            String query = "INSERT INTO PLEX VALUES('" +address.getText() + "', '" + unit.getText() + "', '" + type.getValue() + "', '---', '" + price.getText()+ "', '" + start.getValue()+ "', '" + end.getValue()+ "', '" + institution.getValue()+ "' )";
            executeQuery(query);
            
        Connection conn = getConnection();
        Statement st = conn.createStatement();
        }
           showDetailReports();
    }
    
    private void executeQuery(String query) throws SQLException{
        Connection conn = getConnection();
        Statement st;

        try{
            st = conn.createStatement();
            st.executeUpdate(query);
            warning.setText("Successfully added a Plex");

        }catch (Exception e){
            e.printStackTrace();
            warning.setText("Successfully added a Plex");
        }
    }
    
    public ObservableList<PlexReport> getPlexReports() throws SQLException{
        ObservableList<PlexReport> reportList = FXCollections.observableArrayList();
        Connection conn = getConnection();
        String query = "SELECT * FROM PLEX";
        Statement st;
        ResultSet rs;

        try{
            st= conn.createStatement();
            rs = st.executeQuery(query);
            PlexReport tr;
            while(rs.next()){
                tr = new PlexReport(rs.getString("address"), rs.getString("type"), rs.getString("name"), rs.getDouble("price"),rs.getDate("mortstart"),rs.getDate("mortend"),rs.getString("institution"),rs.getString("unit"));
                reportList.add(tr);

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        ObservableList<String> addressList = FXCollections.observableArrayList();

        
        String query1 = "SELECT * FROM FINANCIAL ";
        Statement st2;
        ResultSet rs2;
        try{
            st2= conn.createStatement();
            rs2 = st2.executeQuery(query1);
            while(rs2.next()){
                addressList.add(rs2.getString("name"));

            }
        }
        catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        institution.setItems(addressList);
        return reportList;
        
        
    }
    
    public void showDetailReports() throws SQLException{
        warning.setText("");
        ObservableList<PlexReport> reportList = getPlexReports();
        
        
        addressCol.setCellValueFactory(new PropertyValueFactory<PlexReport, String>("address"));
        unitCol.setCellValueFactory(new PropertyValueFactory<PlexReport, String>("unit"));
        typeCol.setCellValueFactory(new PropertyValueFactory<PlexReport, String>("type"));
        nameCol.setCellValueFactory(new PropertyValueFactory<PlexReport, String>("name"));
        priceCol.setCellValueFactory(new PropertyValueFactory<PlexReport, String>("price"));
        mortstartCol.setCellValueFactory(new PropertyValueFactory<PlexReport, String>("start"));
        mortendCol.setCellValueFactory(new PropertyValueFactory<PlexReport, String>("end"));
        institutionCol.setCellValueFactory(new PropertyValueFactory<PlexReport, String>("institution"));


        plexReport.setItems(reportList);
    }
    public void deletePlex()throws SQLException{
        
        Connection conn = getConnection();
        String plexName = address.getText();
        String plexUnit = unit.getText();
        String query2="DELETE FROM TENANTS WHERE ADDRESS LIKE('"+plexName+"') AND UNIT LIKE ('"+plexUnit+"')";
        String query = "DELETE FROM PLEX WHERE ADDRESS LIKE('"+plexName+"') AND UNIT LIKE ('"+plexUnit+"')";
        
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query2);

            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure address entered exists");
            }
            else
            {
                warning.setText("You've deleted "+rs+" row");

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
         showDetailReports();
    }
    
    public void modifyPlex()throws SQLException{
        
        Connection conn = getConnection();
        String plexAdd = address.getText();
        String plexUnit = unit.getText();
        String plexPrice = price.getText();
        LocalDate plexStart = start.getValue();
        LocalDate plexEnd = end.getValue();
        Object plexInst = institution.getValue();
        
        

     
        
        String query = "UPDATE PLEX SET PRICE= "+plexPrice+ ", MORTSTART= '"+plexStart+"', MORTEND= '"+plexEnd+ "', INSTITUTION= '"+plexInst+ "' WHERE ADDRESS LIKE ('"+plexAdd+"') AND UNIT LIKE ('"+plexUnit+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure address number entered is a used address");
            }
            else
            {
                warning.setText("You've modified "+rs+" row");

            }

        } catch (Exception throwables) {
            warning.setText("You might've entered an address that doesn't exist, try again");
            System.out.println(throwables.getMessage());
        }
         showDetailReports();
    }
    public Connection getConnection() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/java_project", "root", "Darling2b88!");
            return connection;
        } catch (SQLException e) {
            System.out.println("Error");
            e.printStackTrace();
            return null;
        }
    }
}


