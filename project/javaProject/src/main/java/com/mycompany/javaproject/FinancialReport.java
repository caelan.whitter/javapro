/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

/**
 *
 * @author Caelan
 */


public class FinancialReport {

    public String name;
    public double rate;
 

    public FinancialReport(String name,double rate) {
    this.name=name;
    this.rate=rate;
    }

    public FinancialReport() {
    }


    public String getName() {
        return this.name;
    }

    public double getRate() {
        return this.rate;
    }
  
    
    public void setName(String name) {
        this.name = name;
    }

  
    public void setRate(double rate) {
        this.rate = rate;
    }


}