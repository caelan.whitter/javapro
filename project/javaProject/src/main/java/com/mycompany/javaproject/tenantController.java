/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
/**
 *
 * @author Caelan
 */

public class tenantController {
    @FXML
    public TextField name;
    

    public String globType;
    @FXML
    public TextField unit;
        
    @FXML
    public TextField phone;
    
    @FXML
    public TextField email;
    
    @FXML
    public TextField rent;
    
    @FXML
    public ChoiceBox type;
    
    @FXML
    public Label warning;
    
    @FXML
    private TableView<TenantReport> tenantReport;
    
    @FXML
    private TableColumn<TenantReport, String> nameCol;
    @FXML
    private TableColumn<TenantReport, String> addressCol;
    @FXML
    private TableColumn<TenantReport, String> unitCol;
    @FXML
    private TableColumn<TenantReport, String> phoneCol;
    @FXML
    private TableColumn<TenantReport, String> emailCol;
    @FXML
    private TableColumn<TenantReport, Double> rentCol;

        @FXML
    public ChoiceBox address;

    ObservableList<String> typeList = FXCollections.observableArrayList("HOUSE","CONDO","DUPLEX","TRIPLEX","QUADRAPLEX","MULTIPLEX");

    @FXML
    private void switchToProperties() throws IOException {
        App.setRoot("propertyView");
    }
    
    @FXML
    private void switchToFinancialInstitutions() throws IOException {
        App.setRoot("financialinstitution");
    }
    
    @FXML
    private void switchToMaintenance() throws IOException {
        App.setRoot("maintenanceView");
    }

    @FXML
    private void switchToContractor() throws IOException {
        App.setRoot("contractorView");
    }
    
    public void submitTenant(ActionEvent actionEvent)throws SQLException{
     insertRecord();
    }
    
    private void insertRecord() throws SQLException{
        
       
        if(name.getText().isEmpty())
        {
            warning.setText("name field is blank");
        }
        else
        {

            if(unit.getText().isEmpty()){
                unit.setText("---");
            }
            if(phone.getText().isEmpty()){
                phone.setText("---");
            }
            if(email.getText().isEmpty()){
                email.setText("---");
            }
            if(rent.getText().isEmpty()){
                rent.setText("0.0");
            }
        if(address.getValue() != null && type.getValue() == "HOUSE" ||type.getValue() == "CONDO")
        {
            String query2 = "UPDATE "+type.getValue()+" SET NAME='"+name.getText()+"' WHERE ADDRESS LIKE ('"+address.getValue()+"')";
               try{     
            executeQuery(query2);
               }
               catch(Exception e){
                    warning.setText("Make sure Type is entered ex: HOUSE, PLEX, CONDO");
                     e.printStackTrace();
               }
               
            String query = "INSERT INTO TENANTS VALUES('" +name.getText() + "', '" + address.getValue() + "', '" + unit.getText() + "', '" + phone.getText() + "', '" + email.getText()+ "', " +rent.getText()+ ", '" + type.getValue()+"')";
        
            executeQuery(query);
        }
        else if(address.getValue() != null && type.getValue() == "DUPLEX" || type.getValue() == "TRIPLEX"|| type.getValue() == "QUADRAPLEX"|| type.getValue() == "MULTIPLEX")
        {
            
            String query2 = "UPDATE PLEX SET NAME='"+name.getText()+"' WHERE ADDRESS LIKE ('"+address.getValue()+"') AND UNIT LIKE ('"+unit.getText()+"')";
            try{     
                   
            executeQuery(query2);
               }
               catch(Exception e){
                    warning.setText("Make sure Type is entered ex: HOUSE, PLEX, CONDO");
                     e.printStackTrace();
               }
            String query = "INSERT INTO TENANTS VALUES('" +name.getText() + "', '" + address.getValue() + "', '" + unit.getText() + "', '" + phone.getText() + "', '" + email.getText()+ "', " +rent.getText()+ ", '" + type.getValue()+"')";
        
            executeQuery(query);
        }
        else
        {
            warning.setText("Make sure Type is entered ex: HOUSE, PLEX, CONDO");

        }


        }
        showDetailReports();
          
    }
    
    private void executeQuery(String query) throws SQLException{
        Connection conn = getConnection();
        Statement st;

        try{
            st = conn.createStatement();
            st.executeUpdate(query);
             warning.setText("Successfully added a Tenant");


        }catch (Exception e){
            warning.setText("Make sure Tenant name is not used");
            e.printStackTrace();
        }
    }
    
    public ObservableList<TenantReport> getTenantReports() throws SQLException{
        type.setItems(typeList);

        ObservableList<TenantReport> reportList = FXCollections.observableArrayList();
        Connection conn = getConnection();
        String query = "SELECT * FROM TENANTS";
        Statement st;
        ResultSet rs;

        try{
            st= conn.createStatement();
            rs = st.executeQuery(query);
            TenantReport tr;
            while(rs.next()){
                tr = new TenantReport(rs.getString("name"), rs.getString("address"), rs.getString("unit"), rs.getString("telephone"), rs.getString("email"),rs.getDouble("rent"));
                reportList.add(tr);

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }

        return reportList;
    }
    
    public void showDetailReports() throws SQLException{
        Connection conn = getConnection();
        warning.setText("");
        ObservableList<TenantReport> reportList = getTenantReports();
        
        nameCol.setCellValueFactory(new PropertyValueFactory<TenantReport, String>("name"));
        addressCol.setCellValueFactory(new PropertyValueFactory<TenantReport, String>("address"));
        unitCol.setCellValueFactory(new PropertyValueFactory<TenantReport, String>("unit"));
        phoneCol.setCellValueFactory(new PropertyValueFactory<TenantReport, String>("telephone"));
        emailCol.setCellValueFactory(new PropertyValueFactory<TenantReport, String>("email"));
        rentCol.setCellValueFactory(new PropertyValueFactory<TenantReport, Double>("rent"));


        tenantReport.setItems(reportList);
        
        ObservableList<String> addressList = FXCollections.observableArrayList();
        addressList.add("---");
        addressList.add("HOUSE");
        
        String query1 = "SELECT * FROM HOUSE WHERE NAME LIKE ('---') ";
        Statement st;
        ResultSet rs;
        try{
            st= conn.createStatement();
            rs = st.executeQuery(query1);
            while(rs.next()){
                addressList.add(rs.getString("address"));

            }
        }
        catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        address.setItems(addressList);
        
        addressList.add("---");

        addressList.add("CONDO");
        
        String query2 = "SELECT * FROM CONDO WHERE NAME LIKE ('---') ";
        Statement st2;
        ResultSet rs2;
        try{
            st2= conn.createStatement();
            rs2 = st2.executeQuery(query2);
            while(rs2.next()){
                addressList.add(rs2.getString("address"));

            }
        }
        catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        address.setItems(addressList);
        addressList.add("---");

        addressList.add("PLEX");
        
        String query3 = "SELECT * FROM PLEX WHERE NAME LIKE ('---') ";
        Statement st3;
        ResultSet rs3;
        try{
            st3= conn.createStatement();
            rs3= st3.executeQuery(query3);
            while(rs3.next()){
                addressList.add(rs3.getString("address"));

            }
        }
        catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        address.setItems(addressList);
        
                
    }
    public void deleteTenant()throws SQLException{
        
        Connection conn = getConnection();
        String tenantName = name.getText();
        String query = "DELETE FROM TENANTS WHERE NAME LIKE('"+tenantName+"')";
        String query2 = "UPDATE HOUSE SET NAME = '---' WHERE NAME LIKE ('"+tenantName+"')";
        String query3 = "UPDATE CONDO SET NAME = '---' WHERE NAME LIKE ('"+tenantName+"')";
        String query4 = "UPDATE PLEX SET NAME = '---' WHERE NAME LIKE ('"+tenantName+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query2);
            rs = st.executeUpdate(query3);
            rs = st.executeUpdate(query4);
            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure name entered exists");
            }
            else
            {
                warning.setText("You've deleted "+rs+" row");

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        showDetailReports();
    }
    
    public void modifyTenant() throws SQLException{
        Connection conn = getConnection();
        String tenantName = name.getText();
        Object tenantAdd = address.getValue();
        String tenantUnit = unit.getText();
        String tenantPhone = phone.getText();
        String tenantEmail = email.getText();
        Object tenantType = type.getValue();
       
        String ttype="";
        String tadd="";
        String tunit="";

        if(rent.getText().isEmpty())
        {
            rent.setText("0.0");
        }
        if(address.getValue() == null)
        {
            String query3 = "SELECT * FROM TENANTS WHERE NAME LIKE('"+tenantName+"')";
            Statement st;
            ResultSet rs;
            try{
                st= conn.createStatement();
                rs = st.executeQuery(query3);
                while(rs.next()){
                    ttype = rs.getString("type");
                    tadd = rs.getString("address");
                    tunit = rs.getString("unit");
                }
            }
            catch (Exception throwables) {
                System.out.println(throwables.getMessage());
            }
            if(ttype.equals("DUPLEX") || ttype.equals("TRIPLEX") || ttype.equals("QUADRAPLEX") || ttype.equals("MULTIPLEX"))
            {
                
                String query2 = "UPDATE PLEX SET NAME='---' WHERE ADDRESS LIKE ('"+tadd+"') AND UNIT LIKE ('"+tunit+"')";
                executeQuery(query2);
            }
            else
            {
            String query2 = "UPDATE "+ttype+" SET NAME='---' WHERE ADDRESS LIKE ('"+tadd+"')";
            executeQuery(query2);
            }
        }
        if (address.getValue() != null)
        {
       
            String query4 = "UPDATE PLEX SET NAME='"+tenantName+"' WHERE ADDRESS LIKE ('"+tenantAdd+"') AND UNIT LIKE ('"+tenantUnit+"')";
            executeQuery(query4);


            String query5 = "UPDATE HOUSE SET NAME='"+tenantName+"' WHERE ADDRESS LIKE ('"+tenantAdd+"')";
            String query6 = "UPDATE CONDO SET NAME='"+tenantName+"' WHERE ADDRESS LIKE ('"+tenantAdd+"')";
            executeQuery(query4);
            executeQuery(query5);
            executeQuery(query6);
        }
         String tenantRent = rent.getText();
        String query = "UPDATE TENANTS SET ADDRESS= '"+tenantAdd+ "',UNIT= '"+tenantUnit+ "',TELEPHONE= '"+tenantPhone+ "', EMAIL= '"+tenantEmail+ "', RENT= "+tenantRent+ " WHERE NAME LIKE ('"+tenantName+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure name entered is a used");
            }
            else
            {
                warning.setText("You've modified "+rs+" row");

            }
           

        } catch (Exception throwables) {
            warning.setText("You might've entered a tenant name that doesn't exist, try again");
            System.out.println(throwables.getMessage());
        }
        showDetailReports();
    }
    
    
    public Connection getConnection() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/java_project", "root", "Darling2b88!");
            return connection;
        } catch (SQLException e) {
            System.out.println("Error");
            e.printStackTrace();
            return null;
        }
    }
}
