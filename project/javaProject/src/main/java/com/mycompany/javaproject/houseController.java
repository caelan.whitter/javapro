/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

/**
 *
 * @author Caelan
 */



import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class houseController {

    
    @FXML
    private TableView<HouseReport> houseReport;
        
    @FXML
    private TableColumn<HouseReport, String> addressCol;

    @FXML
    private TableColumn<HouseReport, String> typeCol;

    @FXML
    private TableColumn<HouseReport, String> nameCol;

    @FXML
    private TableColumn<HouseReport, String> priceCol;

    @FXML
    private TableColumn<HouseReport, String> mortstartCol;
    
    @FXML
    private TableColumn<HouseReport, String> mortendCol;

    @FXML
    private TableColumn<HouseReport, String> institutionCol;

    @FXML
    private TextField address;
    


    @FXML
    public TextField price;

    @FXML
    public DatePicker start;

    @FXML
    public DatePicker end;

    @FXML
    public ChoiceBox institution;
    
    @FXML
    public Label warning;

    @FXML
    public Label type;
    

    @FXML
    private void switchToContractor() throws IOException {
        App.setRoot("contractorView");
    }

    @FXML
    private void switchToFinancialInstitutions() throws IOException {
        App.setRoot("financialinstitution");
    }

    @FXML
    private void switchToMaintenance() throws IOException {
        App.setRoot("maintenanceView");
    }

    @FXML
    void switchToProperties() throws IOException {
        App.setRoot("propertyView");
    }

    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("tenantView");
    }

    
        public void submitHouse(ActionEvent actionEvent)throws SQLException{
     insertRecord();
    }
    
    private void insertRecord() throws SQLException{
        
        
        if(address.getText().isEmpty() && type.getText().isEmpty() && price.getText().isEmpty() && start.getValue() == null && end.getValue() == null && institution.getValue() == null)
        {
            warning.setText("one or more field is blank");
        }

        else
        {
            String query = "INSERT INTO HOUSE VALUES('" +address.getText() + "', '" + type.getText() + "', '---', '" + price.getText()+ "', '" + start.getValue()+ "', '" + end.getValue()+ "', '" + institution.getValue()+ "' )";
            executeQuery(query);
            warning.setText("Successfully added a House");
        Connection conn = getConnection();
        Statement st = conn.createStatement();
        }
           showDetailReports();
    }
    
    private void executeQuery(String query) throws SQLException{
        Connection conn = getConnection();
        Statement st;

        try{
            st = conn.createStatement();
            st.executeUpdate(query);


        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public ObservableList<HouseReport> getHouseReports() throws SQLException{
        ObservableList<HouseReport> reportList = FXCollections.observableArrayList();
        Connection conn = getConnection();
        String query = "SELECT * FROM HOUSE";
        Statement st;
        ResultSet rs;

        try{
            st= conn.createStatement();
            rs = st.executeQuery(query);
            HouseReport tr;
            while(rs.next()){
                tr = new HouseReport(rs.getString("address"), rs.getString("type"), rs.getString("name"), rs.getDouble("price"),rs.getDate("mortstart"),rs.getDate("mortend"),rs.getString("institution"));
                reportList.add(tr);

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        ObservableList<String> addressList = FXCollections.observableArrayList();

        
        String query1 = "SELECT * FROM FINANCIAL ";
        Statement st2;
        ResultSet rs2;
        try{
            st2= conn.createStatement();
            rs2 = st2.executeQuery(query1);
            while(rs2.next()){
                addressList.add(rs2.getString("name"));

            }
        }
        catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        institution.setItems(addressList);
        return reportList;
    }
    
    public void showDetailReports() throws SQLException{
        warning.setText("");
        ObservableList<HouseReport> reportList = getHouseReports();
        
        
        addressCol.setCellValueFactory(new PropertyValueFactory<HouseReport, String>("address"));
        typeCol.setCellValueFactory(new PropertyValueFactory<HouseReport, String>("type"));
        nameCol.setCellValueFactory(new PropertyValueFactory<HouseReport, String>("name"));
        priceCol.setCellValueFactory(new PropertyValueFactory<HouseReport, String>("price"));
        mortstartCol.setCellValueFactory(new PropertyValueFactory<HouseReport, String>("start"));
        mortendCol.setCellValueFactory(new PropertyValueFactory<HouseReport, String>("end"));
        institutionCol.setCellValueFactory(new PropertyValueFactory<HouseReport, String>("institution"));


        houseReport.setItems(reportList);
    }
    public void deleteHouse()throws SQLException{
        
        Connection conn = getConnection();
        String houseName = address.getText();
        String query = "DELETE FROM HOUSE WHERE ADDRESS LIKE('"+houseName+"')";
        
        String query2="DELETE FROM TENANTS WHERE ADDRESS LIKE('"+houseName+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query2);

            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure address entered exists");
            }
            else
            {
                warning.setText("You've deleted "+rs+" row");

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
         showDetailReports();
    }
    
    public void modifyHouse()throws SQLException{
        
        Connection conn = getConnection();
        String houseAdd = address.getText();
        
        String housePrice = price.getText();
        LocalDate houseStart = start.getValue();
        LocalDate houseEnd = end.getValue();
        Object houseInst = institution.getValue();
        String houseType = type.getText();
        

     
        
        String query = "UPDATE HOUSE SET TYPE= '"+houseType+ "',PRICE= "+housePrice+ ", MORTSTART= '"+houseStart+"', MORTEND= '"+houseEnd+ "', INSTITUTION= '"+houseInst+ "' WHERE ADDRESS LIKE ('"+houseAdd+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure address number entered is a used address");
            }
            else
            {
                warning.setText("You've modified "+rs+" row");

            }

        } catch (Exception throwables) {
            warning.setText("You might've entered an address that doesn't exist, try again");
            System.out.println(throwables.getMessage());
        }
         showDetailReports();
    }
    public Connection getConnection() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/java_project", "root", "Darling2b88!");
            return connection;
        } catch (SQLException e) {
            System.out.println("Error");
            e.printStackTrace();
            return null;
        }
    }
}


