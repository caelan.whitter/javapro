/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

/**
 *
 * @author Caelan
 */



import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class condoController {

    
    @FXML
    private TableView<CondoReport> condoReport;
        
    @FXML
    private TableColumn<CondoReport, String> addressCol;

    @FXML
    private TableColumn<CondoReport, String> feesCol;

    @FXML
    private TableColumn<CondoReport, String> typeCol;

    @FXML
    private TableColumn<CondoReport, String> nameCol;

    @FXML
    private TableColumn<CondoReport, String> priceCol;

    @FXML
    private TableColumn<CondoReport, String> mortstartCol;
    
    @FXML
    private TableColumn<CondoReport, String> mortendCol;

    @FXML
    private TableColumn<CondoReport, String> institutionCol;

    @FXML
    private TextField address;

    @FXML
    private TextField fees;
    


    @FXML
    public TextField price;

    @FXML
    public DatePicker start;

    @FXML
    public DatePicker end;

    @FXML
    public ChoiceBox institution;
    
    @FXML
    public Label warning;

    @FXML
    public Label type;
    
    


    @FXML
    private void switchToContractor() throws IOException {
        App.setRoot("contractorView");
    }

    @FXML
    private void switchToFinancialInstitutions() throws IOException {
        App.setRoot("financialinstitution");
    }

    @FXML
    private void switchToMaintenance() throws IOException {
        App.setRoot("maintenanceView");
    }

    @FXML
    void switchToProperties() throws IOException {
        App.setRoot("propertyView");
    }

    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("tenantView");
    }


    
        public void submitCondo(ActionEvent actionEvent)throws SQLException{
     insertRecord();
    }
    
    private void insertRecord() throws SQLException{
        
        
        if(address.getText().isEmpty() && fees.getText().isEmpty() && type.getText().isEmpty() && price.getText().isEmpty() && start.getValue() == null && end.getValue() == null && institution.getValue() == null)
        {
            warning.setText("one or more field is blank");
        }
        else
        {
            String query = "INSERT INTO CONDO VALUES('" +address.getText() + "', '" + type.getText() + "', '" + fees.getText() + "', '---', '" + price.getText()+ "', '" + start.getValue()+ "', '" + end.getValue()+ "', '" + institution.getValue()+ "' )";
            executeQuery(query);
            warning.setText("Successfully added a Condo");
        Connection conn = getConnection();
        Statement st = conn.createStatement();
        }
           showDetailReports();
    }
    
    private void executeQuery(String query) throws SQLException{
        Connection conn = getConnection();
        Statement st;

        try{
            st = conn.createStatement();
            st.executeUpdate(query);


        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public ObservableList<CondoReport> getCondoReports() throws SQLException{
        ObservableList<CondoReport> reportList = FXCollections.observableArrayList();
        Connection conn = getConnection();
        String query = "SELECT * FROM CONDO";
        Statement st;
        ResultSet rs;

        try{
            st= conn.createStatement();
            rs = st.executeQuery(query);
            CondoReport tr;
            while(rs.next()){
                tr = new CondoReport(rs.getString("address"), rs.getString("type"), rs.getString("name"), rs.getDouble("price"),rs.getDate("mortstart"),rs.getDate("mortend"),rs.getString("institution"),rs.getDouble("fees"));
                reportList.add(tr);

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        ObservableList<String> addressList = FXCollections.observableArrayList();

        
        String query1 = "SELECT * FROM FINANCIAL ";
        Statement st2;
        ResultSet rs2;
        try{
            st2= conn.createStatement();
            rs2 = st2.executeQuery(query1);
            while(rs2.next()){
                addressList.add(rs2.getString("name"));

            }
        }
        catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        institution.setItems(addressList);
        return reportList;
    }
    
    public void showDetailReports() throws SQLException{
        warning.setText("");
        ObservableList<CondoReport> reportList = getCondoReports();
        
        
        addressCol.setCellValueFactory(new PropertyValueFactory<CondoReport, String>("address"));
        feesCol.setCellValueFactory(new PropertyValueFactory<CondoReport, String>("fees"));
        typeCol.setCellValueFactory(new PropertyValueFactory<CondoReport, String>("type"));
        nameCol.setCellValueFactory(new PropertyValueFactory<CondoReport, String>("name"));
        priceCol.setCellValueFactory(new PropertyValueFactory<CondoReport, String>("price"));
        mortstartCol.setCellValueFactory(new PropertyValueFactory<CondoReport, String>("start"));
        mortendCol.setCellValueFactory(new PropertyValueFactory<CondoReport, String>("end"));
        institutionCol.setCellValueFactory(new PropertyValueFactory<CondoReport, String>("institution"));


        condoReport.setItems(reportList);
    }
    public void deleteCondo()throws SQLException{
        
        Connection conn = getConnection();
        String condoName = address.getText();
        String query = "DELETE FROM CONDO WHERE ADDRESS LIKE('"+condoName+"')";
        String query2= "DELETE FROM TENANTS WHERE ADDRESS LIKE('"+condoName+"')";
       
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query2);

            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure address entered exists");
            }
            else
            {
                warning.setText("You've deleted "+rs+" row");

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
         showDetailReports();
    }
    
    public void modifyCondo() throws SQLException{
        Connection conn = getConnection();
        String condoAdd = address.getText();
        String condoFee = fees.getText();
        String condoPrice = price.getText();
        LocalDate condoStart = start.getValue();
        LocalDate condoEnd = end.getValue();
        Object condoInst = institution.getValue();
        String condoType = type.getText();
        
                
        String query = "UPDATE CONDO SET TYPE= '"+condoType+ "',FEES="+condoFee+",PRICE= "+condoPrice+ ", MORTSTART= '"+condoStart+"', MORTEND= '"+condoEnd+ "', INSTITUTION= '"+condoInst+ "' WHERE ADDRESS LIKE ('"+condoAdd+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure address number entered is a used address");
            }
            else
            {
                warning.setText("You've modified "+rs+" row");

            }

        } catch (Exception throwables) {
            warning.setText("You might've entered an address that doesn't exist, try again");
            System.out.println(throwables.getMessage());
        }
         showDetailReports();
        
    }
    public Connection getConnection() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/java_project", "root", "Darling2b88!");
            return connection;
        } catch (SQLException e) {
            System.out.println("Error");
            e.printStackTrace();
            return null;
        }
    }
}


