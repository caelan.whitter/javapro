/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Caelan
 */
public class financialController {
        @FXML
    public TextField name;
    
    @FXML
    public TextField rate;

    @FXML
    public Label warning;
    
    @FXML
    private TableView<FinancialReport> financialReport;
    
    @FXML
    private TableColumn<FinancialReport, String> nameCol;
    @FXML
    private TableColumn<FinancialReport, Double> rateCol;

    
    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("tenantView");
    }

    @FXML
    private void switchToProperties() throws IOException {
        App.setRoot("propertyView");
    }

    @FXML
    private void switchToMaintenance() throws IOException {
        App.setRoot("maintenanceView");
    }

    @FXML
    private void switchToContractor() throws IOException {
        App.setRoot("contractorView");
    }
    
    public void submitFinancial(ActionEvent actionEvent)throws SQLException{
     insertRecord();
    }
    
    private void insertRecord() throws SQLException{
        
        
        if(name.getText().isEmpty() && rate.getText().isEmpty() )
        {
            warning.setText("one or more field is blank");
        }
        else
        {
            String query = "INSERT INTO FINANCIAL VALUES('" +name.getText() + "', " + rate.getText() +" )";
            executeQuery(query);
            warning.setText("Successfully added an Institution");
        Connection conn = getConnection();
        Statement st = conn.createStatement();
        }
                showDetailReports();

          
    }
    
    private void executeQuery(String query) throws SQLException{
        Connection conn = getConnection();
        Statement st;

        try{
            st = conn.createStatement();
            st.executeUpdate(query);


        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public ObservableList<FinancialReport> getFinancialReports() throws SQLException{
        ObservableList<FinancialReport> reportList = FXCollections.observableArrayList();
        Connection conn = getConnection();
        String query = "SELECT * FROM FINANCIAL";
        Statement st;
        ResultSet rs;

        try{
            st= conn.createStatement();
            rs = st.executeQuery(query);
            FinancialReport tr;
            while(rs.next()){
                tr = new FinancialReport(rs.getString("name"), rs.getDouble("rate"));
                reportList.add(tr);

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }

        return reportList;
    }
    
    public void showDetailReports() throws SQLException{
        warning.setText("");
        ObservableList<FinancialReport> reportList = getFinancialReports();
        
        nameCol.setCellValueFactory(new PropertyValueFactory<FinancialReport, String>("name"));
        rateCol.setCellValueFactory(new PropertyValueFactory<FinancialReport, Double>("rate"));


        financialReport.setItems(reportList);
    }
    public void deleteFinancial()throws SQLException{
        
        Connection conn = getConnection();
        String financialName = name.getText();
        String query2 = "UPDATE HOUSE SET institution = '---' WHERE institution LIKE ('"+financialName+"')";
        String query3 = "UPDATE CONDO SET institution = '---' WHERE institution LIKE ('"+financialName+"')";
        String query4 = "UPDATE PLEX SET institution = '---' WHERE institution LIKE ('"+financialName+"')";

        String query = "DELETE FROM FINANCIAL WHERE NAME LIKE('"+financialName+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query2);
            rs = st.executeUpdate(query3);
            rs = st.executeUpdate(query4);
            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure name entered exists");
            }
            else
            {
                warning.setText("You've deleted "+rs+" row");

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        showDetailReports();
    }
    
    public void modifyFinancial() throws SQLException{
        Connection conn = getConnection();
        String financialName = name.getText();
        String financialRate = rate.getText();

        String query = "UPDATE FINANCIAL SET RATE= '"+financialRate+ "' WHERE NAME LIKE ('"+financialName+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query);
            
                    if (rs == 0)
            {
                warning.setText("Make sure name entered exists");
            }
            else
            {
                warning.setText("You've modified "+rs+" row");

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
                showDetailReports();

    }
    public Connection getConnection() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/java_project", "root", "Darling2b88!");
            return connection;
        } catch (SQLException e) {
            System.out.println("Error");
            e.printStackTrace();
            return null;
        }
    }
        
     
}
