/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Caelan
 */
public class contractorController {
    @FXML
    public TextField name;
    
    @FXML
    public TextField type;
    
    @FXML
    public TextField phone;
    
    @FXML
    public TextField email;
    
    @FXML
    public Label warning;
    
    @FXML
    private TableView<ContractorReport> contractorReport;
    
    @FXML
    private TableColumn<ContractorReport, String> nameCol;
    @FXML
    private TableColumn<ContractorReport, String> typeCol;
    @FXML
    private TableColumn<ContractorReport, String> phoneCol;
    @FXML
    private TableColumn<ContractorReport, String> emailCol;
    
    @FXML
        private void switchToTenants() throws IOException {
        App.setRoot("tenantView");
    }

    @FXML
        private void switchToProperties() throws IOException {
        App.setRoot("propertyView");
    }
    @FXML
        private void switchToFinancialInstitutions() throws IOException {
        App.setRoot("financialinstitution");
    }
    @FXML
        private void switchToMaintenance() throws IOException {
        App.setRoot("maintenanceView");
    }
        
    public void submitContractor(ActionEvent actionEvent)throws SQLException{
     insertRecord();
    }
    
    private void insertRecord() throws SQLException{
        
        
        if(name.getText().isEmpty() && type.getText().isEmpty() && phone.getText().isEmpty() && email.getText().isEmpty())
        {
            warning.setText("one or more field is blank");
        }
        else
        {
            String query = "INSERT INTO CONTRACTORS VALUES('" +name.getText() + "', '" + type.getText() + "', '" + phone.getText() + "', '" + email.getText()+ "' )";
            executeQuery(query);
            warning.setText("Successfully added a Contractor");
        Connection conn = getConnection();
        Statement st = conn.createStatement();
        }
           showDetailReports();
    }
    
    public ObservableList<ContractorReport> getContractorReports() throws SQLException{
        ObservableList<ContractorReport> reportList = FXCollections.observableArrayList();
        Connection conn = getConnection();
        String query = "SELECT * FROM CONTRACTORS";
        Statement st;
        ResultSet rs;

        try{
            st= conn.createStatement();
            rs = st.executeQuery(query);
            ContractorReport tr;
            while(rs.next()){
                tr = new ContractorReport(rs.getString("name"), rs.getString("type"),rs.getString("telephone"), rs.getString("email"));
                reportList.add(tr);
      
            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }

        return reportList;
    }
    
    public void showDetailReports() throws SQLException{
        warning.setText("");
        ObservableList<ContractorReport> reportList = getContractorReports();
        
        nameCol.setCellValueFactory(new PropertyValueFactory<ContractorReport, String>("name"));
        typeCol.setCellValueFactory(new PropertyValueFactory<ContractorReport, String>("type"));
        phoneCol.setCellValueFactory(new PropertyValueFactory<ContractorReport, String>("telephone"));
        emailCol.setCellValueFactory(new PropertyValueFactory<ContractorReport, String>("email"));

        contractorReport.setItems(reportList);
    }
    
    public void modifyContractor()throws SQLException{
        
        Connection conn = getConnection();
        String contractorName = name.getText();
        String contractorType = type.getText();
        String contractorTel = phone.getText();
        String contractorEmail = email.getText();
        String query = "UPDATE CONTRACTORS SET TYPE= '"+contractorType+ "', TELEPHONE= '"+contractorTel+ "', EMAIL= '"+contractorEmail+"' WHERE NAME LIKE ('"+contractorName+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query);
            
                    if (rs == 0)
            {
                warning.setText("Make sure name entered exists");
            }
            else
            {
                warning.setText("You've modified "+rs+" row");

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
     showDetailReports();
    }
    
    public void deleteContractor() throws SQLException{
        Connection conn = getConnection();
        String contractorName = name.getText();
        String query2 = "UPDATE MAINTENANCE SET CONTRACTOR = '---' WHERE CONTRACTOR LIKE ('"+contractorName+"')";
        String query = "DELETE FROM CONTRACTORS WHERE NAME LIKE('"+contractorName+"')";
        Statement st;
        int rs;
                try{
            st= conn.createStatement();
            rs = st.executeUpdate(query2);
            rs = st.executeUpdate(query);
            
            if (rs == 0)
            {
                warning.setText("Make sure name entered exists");
            }
            else
            {
                warning.setText("You've deleted "+rs+" row");

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
                 showDetailReports();
    }
        
    private void executeQuery(String query) throws SQLException{
        Connection conn = getConnection();
        Statement st;

        try{
            st = conn.createStatement();
            st.executeUpdate(query);


        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    
    public Connection getConnection() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/java_project", "root", "Darling2b88!");
            return connection;
        } catch (SQLException e) {
            System.out.println("Error");
            e.printStackTrace();
            return null;
        }
    }
}
