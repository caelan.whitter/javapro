/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

/**
 *
 * @author Caelan
 */


public class TenantReport {

    public String name;
    public String address;
    public String unit;
    public String telephone;
    public String email;
    public double rent;
 

    public TenantReport(String name,String address,String unit,String telephone,String email,double rent) {
    this.name=name;
    this.address=address;
    this.unit=unit;
    this.telephone=telephone;
    this.email=email;
    this.rent=rent;
    }

    public TenantReport() {
    }


    public String getName() {
        return this.name;
    }

    public String getAddress() {
        return this.address;
    }
    
    public String getUnit() {
        return this.unit;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public String getEmail() {
        return this.email;
    }
    
    public double getRent(){
        return this.rent;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void setRent(double rent){
        this.rent=rent;
    }


}