/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

import java.sql.Date;

/**
 *
 * @author Caelan
 */
public class PropertyReport {
    public String address;
    public String type;
    public String name;
    public double price;
    public Date start;
    public Date end;
    public String institution;
 

    public PropertyReport(String address,String type,String name,double price,Date start,Date end,String institution) {
    this.address=address;
    this.type=type;
    this.name=name;
    this.price=price;
    this.start=start;
    this.end=end;
    this.institution=institution;
    }

    public PropertyReport() {
    }


    public String getAddress() {
        return this.address;
    }
    
    public String getType() {
        return this.type;
    }
    
    public String getName() {
        return this.name;
    }

    public double getPrice(){
        return this.price;
    }

    public Date getStart() {
        return this.start;
    }

    public Date getEnd() {
        return this.end;
    }
    public String getInstitution(){
        return this.institution;
    }
    

    
    public void setName(String name) {
        this.name = name;
    }
    public void setAddress(String address) {
        this.address=address;
    }
    
    public void setType(String type) {
        this.type=type;
    }
    

    public void setPrice(double price){
        this.price=price;
    }

    public void setStart(Date start) {
        this.start=start;
    }

    public void setEnd(Date end) {
        this.end=end;
    }
    
    public void setInstitution(String institution){
        this.institution=institution;
    }
}
