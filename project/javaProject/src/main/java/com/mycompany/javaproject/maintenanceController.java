/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Caelan
 */
public class maintenanceController {
    
            @FXML
    public TextField id; 
    
    @FXML
    public ChoiceBox address;
    
    @FXML
    public TextField unit;
            
        @FXML
    public ChoiceBox name;
    
    @FXML
    public TextField type;
    
    @FXML
    public TextField duration;
        
    @FXML
    public TextField cost;
    
    @FXML
    public DatePicker date;
    
    @FXML
    public Label warning;
    
    @FXML
    private TableView<MaintenanceReport> maintenanceReport;
    
    @FXML
    private TableColumn<MaintenanceReport, String> idCol;
    @FXML
    private TableColumn<MaintenanceReport, String> addressCol;
    @FXML
    private TableColumn<MaintenanceReport, String> unitCol;
    @FXML
    private TableColumn<MaintenanceReport, String> nameCol;
    @FXML
    private TableColumn<MaintenanceReport, String> typeCol;
    @FXML
    private TableColumn<MaintenanceReport, String> dateCol ;
    @FXML
    private TableColumn<MaintenanceReport, String> durationCol;
    @FXML
    private TableColumn<MaintenanceReport, Double> costCol;
    
    
                    @FXML
        private void switchToTenants() throws IOException {
        App.setRoot("tenantView");
    }

            @FXML
        private void switchToProperties() throws IOException {
        App.setRoot("propertyView");
    }
                @FXML
        private void switchToFinancialInstitutions() throws IOException {
        App.setRoot("financialinstitution");
    }


                        @FXML
        private void switchToContractor() throws IOException {
        App.setRoot("contractorView");
    } 
        


  public void submitMaintenance(ActionEvent actionEvent)throws SQLException{
     insertRecord();
    }

    
    private void insertRecord() throws SQLException{
        
        
        if(id.getText().isEmpty() && address.getValue()==null && name.getValue()==null && type.getText().isEmpty() && duration.getText().isEmpty() && cost.getText().isEmpty() && date.getValue() == null)
        {
            warning.setText("one or more field is blank");
        }
        else
        {
            String query = "INSERT INTO MAINTENANCE VALUES('" +id.getText() + "', '" +address.getValue() + "', '"+unit.getText() + "', '" +name.getValue() + "', '" + type.getText() + "', '" + date.getValue()+  "', '" + duration.getText() + "', " + cost.getText() +" )";
            executeQuery(query);
            
        Connection conn = getConnection();
        Statement st = conn.createStatement();
        }
           showDetailReports();
    }
    
    private void executeQuery(String query) throws SQLException{
        Connection conn = getConnection();
        Statement st;

        try{
            st = conn.createStatement();
            st.executeUpdate(query);
            warning.setText("Successfully added a Maintenance");

        }catch (Exception e){
            warning.setText("You might've entered a Contractor name that doesn't exist, try again");
            e.printStackTrace();
        }
    }
    
    public ObservableList<MaintenanceReport> getMaintenanceReports() throws SQLException{
        ObservableList<MaintenanceReport> reportList = FXCollections.observableArrayList();
        Connection conn = getConnection();
        String query = "SELECT * FROM MAINTENANCE";
        Statement st;
        ResultSet rs;

        try{
            st= conn.createStatement();
            rs = st.executeQuery(query);
            MaintenanceReport tr;
            while(rs.next()){
                tr = new MaintenanceReport(rs.getString("id"),rs.getString("address"),rs.getString("unit"),rs.getString("contractor"), rs.getString("type"), rs.getDate("dates"), rs.getString("duration"), rs.getDouble("cost"));
                reportList.add(tr);

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }

        return reportList;
    }
    
    public void showDetailReports() throws SQLException{
                Connection conn = getConnection();

        warning.setText("");
        ObservableList<MaintenanceReport> reportList = getMaintenanceReports();
        
        idCol.setCellValueFactory(new PropertyValueFactory<MaintenanceReport, String>("id"));
        addressCol.setCellValueFactory(new PropertyValueFactory<MaintenanceReport, String>("address"));
        unitCol.setCellValueFactory(new PropertyValueFactory<MaintenanceReport, String>("unit"));
        nameCol.setCellValueFactory(new PropertyValueFactory<MaintenanceReport, String>("name"));
        typeCol.setCellValueFactory(new PropertyValueFactory<MaintenanceReport, String>("type"));
        dateCol.setCellValueFactory(new PropertyValueFactory<MaintenanceReport, String>("date"));
        durationCol.setCellValueFactory(new PropertyValueFactory<MaintenanceReport, String>("duration"));
        costCol.setCellValueFactory(new PropertyValueFactory<MaintenanceReport, Double>("cost"));

        maintenanceReport.setItems(reportList);
        
         ObservableList<String> addressList = FXCollections.observableArrayList();
        addressList.add("---");
        addressList.add("HOUSE");
        
        String query1 = "SELECT * FROM HOUSE";
        Statement st;
        ResultSet rs;
        try{
            st= conn.createStatement();
            rs = st.executeQuery(query1);
            while(rs.next()){
                addressList.add(rs.getString("address"));

            }
        }
        catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        address.setItems(addressList);
        
        addressList.add("---");

        addressList.add("CONDO");
        
        String query2 = "SELECT * FROM CONDO";
        Statement st2;
        ResultSet rs2;
        try{
            st2= conn.createStatement();
            rs2 = st2.executeQuery(query2);
            while(rs2.next()){
                addressList.add(rs2.getString("address"));

            }
        }
        catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        address.setItems(addressList);
        addressList.add("---");

        addressList.add("PLEX");
        
        String query3 = "SELECT * FROM PLEX";
        Statement st3;
        ResultSet rs3;
        try{
            st3= conn.createStatement();
            rs3= st3.executeQuery(query3);
            while(rs3.next()){
                addressList.add(rs3.getString("address"));

            }
        }
        catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        address.setItems(addressList);
        
        ObservableList<String> nameList = FXCollections.observableArrayList();
        String query4 = "SELECT * FROM CONTRACTORS";
        Statement st4;
        ResultSet rs4;
        try{
            st4 = conn.createStatement();
            rs4 = st4.executeQuery(query4);
            while(rs4.next()){
                nameList.add(rs4.getString("name"));

            }
        }
        catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
        name.setItems(nameList);
    }
    public void deleteMaintenance()throws SQLException{
        
        Connection conn = getConnection();
        String maintid = id.getText();
        String query = "DELETE FROM maintenance WHERE ID LIKE('"+maintid+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure identification number entered is a used id");
            }
            else
            {
                warning.setText("You've deleted "+rs+" row");

            }

        } catch (Exception throwables) {
            System.out.println(throwables.getMessage());
        }
     showDetailReports();
    }
        public void modifyMaintenance()throws SQLException{
        
        Connection conn = getConnection();
        String maintId = id.getText();
        Object maintAdd = address.getValue();
        String maintUnit = unit.getText();
        Object maintName = name.getValue();
        String maintType = type.getText();
        String maintDur = duration.getText();
        LocalDate maintDate = date.getValue();
        String maintCost = cost.getText();
        
        String query = "UPDATE MAINTENANCE SET ADDRESS= '"+maintAdd+ "',UNIT= '"+maintUnit+ "',CONTRACTOR= '"+maintName+ "', TYPE= '"+maintType+ "', DATES= '"+maintDate+"', DURATION= '"+maintDur+ "', COST= '"+maintCost+ "' WHERE ID LIKE ('"+maintId+"')";
        Statement st;
        int rs;

        try{
            st= conn.createStatement();
            rs = st.executeUpdate(query);
            if (rs == 0)
            {
                warning.setText("Make sure identification number entered is a used id");
            }
            else
            {
                warning.setText("You've modified "+rs+" row");

            }

        } catch (Exception throwables) {
            warning.setText("You might've entered a Contractor name that doesn't exist, try again");
            System.out.println(throwables.getMessage());
        }
         showDetailReports();
    }
    public Connection getConnection() {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/java_project", "root", "Darling2b88!");
            return connection;
        } catch (SQLException e) {
            System.out.println("Error");
            e.printStackTrace();
            return null;
        }
    }
}
