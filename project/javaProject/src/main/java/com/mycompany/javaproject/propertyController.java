/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

import java.io.IOException;
import javafx.fxml.FXML;

/**
 *
 * @author Caelan
 */
public class propertyController {
    
    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("tenantView");
    }
    
    @FXML
    private void switchToProperties() throws IOException {
        App.setRoot("propertyView");
    }

    @FXML
    private void switchToFinancialInstitutions() throws IOException {
        App.setRoot("financialinstitution");
    }
    
    @FXML
    private void switchToMaintenance() throws IOException {
        App.setRoot("maintenanceView");
    }

    @FXML
        private void switchToContractor() throws IOException {
        App.setRoot("contractorView");
    } 
        
    @FXML
        private void switchToHouse() throws IOException {
        App.setRoot("houseView");
    } 
        
    @FXML
    private void switchToPlex() throws IOException {
        App.setRoot("plexView");
    } 
    
    @FXML
    private void switchToCondo() throws IOException {
        App.setRoot("condoView");
    } 
}
