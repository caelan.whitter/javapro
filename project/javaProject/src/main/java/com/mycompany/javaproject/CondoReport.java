/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

import java.sql.Date;

/**
 *
 * @author Caelan
 */
public class CondoReport extends PropertyReport{
    
    public double fees;
    
    public CondoReport(String address,String type,String name,double price,Date start,Date end,String institution,double fees) {
        super(address,type,name,price,start,end,institution);
        this.fees=fees;
    }  
        
        
    public CondoReport() {
    }
    
    public double getFees(){
        return this.fees;
    }
    
    public void setFees(double fees){
        this.fees=fees;
    }
    
}
