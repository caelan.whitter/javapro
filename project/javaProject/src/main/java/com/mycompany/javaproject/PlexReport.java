/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

import java.sql.Date;

/**
 *
 * @author Caelan
 */
public class PlexReport extends PropertyReport{
    
    public String unit;
    
    public PlexReport(String address,String type,String name,double price,Date mortstart,Date mortend,String institution,String unit) {
        super(address,type,name,price,mortstart,mortend,institution);
        this.unit=unit;
    }  
        
        
    public PlexReport() {
    }
    
    public String getUnit(){
        return this.unit;
    }
    
    public void setUnit(String unit){
        this.unit=unit;
    }
    
}
