/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaproject;

/**
 *
 * @author Caelan
 */


public class ContractorReport {

    public String name;
    public String type;
    public String telephone;
    public String email;
 

    public ContractorReport(String name,String type,String telephone,String email) {
    this.name=name;
    this.type=type;
    this.telephone=telephone;
    this.email=email;
    }

    public ContractorReport() {
    }


    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public String getEmail() {
        return this.email;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}