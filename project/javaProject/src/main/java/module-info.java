module com.mycompany.javaproject {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;

    opens com.mycompany.javaproject to javafx.fxml;
    exports com.mycompany.javaproject;
}
